import java.util.*;

public class ControleAluno{
	
	private ArrayList<Aluno> listaAlunos;

	public ControleAluno(){
		listaAlunos = new ArrayList<Aluno>();
	} 

	public String adicionar(Aluno umAluno){
		String mensagem = "Aluno adicionado com Sucesso!";
		listaAlunos.add(umAluno);
		return mensagem;
	}
	
	public String remover(Aluno umAluno){
		String mensagem = "Aluno removido com Sucesso!";
		listaAlunos.remove(umAluno);
		return mensagem;
	}

	public Aluno pesquisarAluno(String umNome){
		for(Aluno umAluno: listaAlunos){
			if(umAluno.getNome().equalsIgnoreCase(umNome)) return umAluno;
			}
	return null;

	}
}
