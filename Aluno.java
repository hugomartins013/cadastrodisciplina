public class Aluno{

	private String nome;
	private String matricula;

	public Aluno(){
	}
	public Aluno (String umNome){
		nome = umNome;
	}

	public Aluno(String umNome, String umaMatricula){
		nome = umNome;
		matricula = umaMatricula;
	}

	public String getNome(){
		return nome;	
	}

	public void setNome(String umNome){
		nome = umNome;
	}


	public String getMatricula(){
		return matricula;
	}

	public void setMatricula(String umaMatricula){
		matricula = umaMatricula;
	}
}
