import java.util.*;
public class ControleDisciplina{

	private ArrayList<Disciplina> listaDisciplinas;

	public ControleDisciplina(){
		 listaDisciplinas = new ArrayList<Disciplina>();
		 //listaDisciplinas = new Arraylist<Disciplina>();
	}

	public String adicionar(Disciplina umaDisciplina){
		String mensagem = "Disciplina Adicionada com Sucesso!";
		listaDisciplinas.add(umaDisciplina);
		return mensagem;
	}

	public String remover(Disciplina umaDisciplina){
		String mensagem = "Disciplina Removida com Sucesso!";
		listaDisciplinas.remove(umaDisciplina);
		return mensagem;
	}

	public Disciplina pesquisarNomeDisciplina(String umNomeDisciplina){
		for(Disciplina umaDisciplina: listaDisciplinas){
			if(umaDisciplina.getNomeDisciplina().equalsIgnoreCase(umNomeDisciplina)) return umaDisciplina;
			}
		return null;
	}
}
